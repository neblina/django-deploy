# Deploy your django project

This code uses ansible to setup a docker project in a server that will
host you django code. The django project should be hosted in some
repository and will be pulled. All configuration options can be defined
in the vars.yml file.

The process of dockerizing a django project was adapted from
[this article](https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/).

It will use nginx+gunicorn for serving and postgresql as the database.

### Dependencies

 - ansible
 - nickjj's docker role (install with `$ ansible-galaxy install nickjj.docker`)

### Usage

 - Adapt your django project to use the environmental variables
   described below.
 - `$ cp vars.yml.example vars.yml` and update all variables
 - `$ ansible-playbook playbook.yml`

### Adapting you django project

* Your django project should use these environmental variables:
 - `SECRET_KEY`
 - `DEBUG`
 - `ALLOWED_HOSTS`
 - `SQL_ENGINE`
 - `SQL_DATABASE`
 - `SQL_USER`
 - `SQL_PASSWORD`
 - `SQL_HOST`
 - `SQL_PORT`

Example database configuration (inside project's settings):

```python
DATABASES = {
    "default": {
        "ENGINE": os.environ.get("SQL_ENGINE", "django.db.backends.sqlite3"),
        "NAME": os.environ.get("SQL_DATABASE", BASE_DIR / "db.sqlite3"),
        "USER": os.environ.get("SQL_USER", "user"),
        "PASSWORD": os.environ.get("SQL_PASSWORD", "password"),
        "HOST": os.environ.get("SQL_HOST", "localhost"),
        "PORT": os.environ.get("SQL_PORT", "5432"),
    }
}
```

* It should contain a requirements.txt file in it's top level folder. To
generate one, use `pip freeze > requirements.txt`.

### Recommended method of keeping secrets under version control

We recommend using gpg to lock secrets and being able to put them under
VC. The provided `gpg-wrapper.sh` should be of use. For that to work,
the user must have a pair of cryptographic keys (to do that, see for
example [this](https://docs.github.com/en/authentication/managing-commit-signature-verification/generating-a-new-gpg-key) link).

To use this wrapper, it should be enough to:

1. In ansible.cfg uncomment `vault_password_file = gpg-wrapper.sh`
2. Create your password file with

```
$ pwgen -n 71 -C | head -n1 | gpg --armor --recipient GPG_ID -e -o vault_pw
```

Where `GPG_ID` is the email or ID associated with the gpg key to be used.
(installing pwgen maybe necessary with, for example, `apt install pwgen`)

3. Run `$ ansible-vault create secrets.yml`, and move the variables
   with sensitive information from `vars.yml` here (remeber to remove
   those from `vars.yml`!). You might want to keep all variables here
   for extra precaution.
4. In playbook.yml, uncomment `secrets.yml` from the `vars` section.

This approach was inspired by [this article](https://disjoint.ca/til/2016/12/14/encrypting-the-ansible-vault-passphrase-using-gpg/).

### In the server

The server will have the following folder structure, supposing that your
project's name is "app":

```
project folder
    app/
        app/
            settings.py
            ...
        ...
    requirements.txt
    Dockerfile
    .env
    docker-compose.yml
    nginx/
        Dockerfile
        nginx.conf
    venv/
```

### Setting up an onion service

1. First, you must have a private key in hands. If you don't maybe use
   something like [mkp244o](https://github.com/cathugger/mkp224o) to
   generate one.
2. Save the base64 representation of the key in the `tor_key` variable
   in `vars.yml` or `secrets.yml`. If you did use mkp244o, you'll have
   to run something like `cat <address>.onion/hs_ed25519_secret_key |
   base64` to get it.
3. Set `use_hidden_service` to `true` in `vars.yml.`

### Further improvements (PR's welcome)

* More detailed options in nginx.conf (maybe check out [gunicorn's
  documentation](https://docs.gunicorn.org/en/latest/deploy.html))
* Use a multi-step build approach (see the [reference article](https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/)).
