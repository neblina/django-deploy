#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

{% if run_collectstatic %}python manage.py collectstatic --noinput{% endif %}

{% if run_makemigrations %}python manage.py makemigrations --noinput{% endif %}

{% if run_migrate %}python manage.py migrate --noinput{% endif %}

{% if run_makemessages %}python manage.py makemessages{% endif %}

{% if run_compilemessages %}python manage.py compilemessages{% endif %}

{% if run_createsuperuser %}python manage.py createsuperuser --noinput{% endif %}

{% if django_extra_commands is defined %}{{ django_extra_commands }}{% endif %}

exec "$@"
