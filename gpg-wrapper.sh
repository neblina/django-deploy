#!/bin/bash

VAULT_PW_FILENAME="vault_pw"
gpg --quiet --batch --use-agent --decrypt $VAULT_PW_FILENAME
